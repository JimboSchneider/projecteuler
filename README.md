# README #

This is a practice project area for Project Euler in a console app.

### What is this repository for? ###

* [Project Euler](https://projecteuler.net/) practice
* [Angular 2.0](https://angular.io/)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install Visual Studio
* Build and run the project
* Right now it's a console app, this is going to be converted to Angular.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jimbo Schneider